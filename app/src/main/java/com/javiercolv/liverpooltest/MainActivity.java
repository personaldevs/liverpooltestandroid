package com.javiercolv.liverpooltest;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.javiercolv.liverpooltest.controller.ProductsAdapter;
import com.javiercolv.liverpooltest.controller.SearchController;
import com.javiercolv.liverpooltest.controller.SuggestionProvider;
import com.javiercolv.liverpooltest.db.DBOpenHelper;
import com.javiercolv.liverpooltest.model.ProductModel;
import com.javiercolv.liverpooltest.services.AsyncTaskConnectService;
import com.javiercolv.liverpooltest.services.ResponseRestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity {

    private List<ProductModel> products;
    private ProductsAdapter productsAdapter;
    SearchController searchController;

    Set<String> historial;
    private SQLiteDatabase innerDB;

    SearchRecentSuggestions suggestions;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        products = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);
        productsAdapter = new ProductsAdapter(MainActivity.this,products);


        progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(10);
        searchController = new SearchController(MainActivity.this, progressBar);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productsAdapter);

        historial = new TreeSet<String>() {
        };

        suggestions = new SearchRecentSuggestions(MainActivity.this,
                SuggestionProvider.AUTHORITY,
                SuggestionProvider.MODE);

        initializeLocalDataBase();
        loadSuggestions();


    }

    private void loadSuggestions() {

        Cursor cursor = innerDB.rawQuery("SELECT id FROM historial", null);
        if (cursor.moveToFirst()) {
            int i=0;
            //cursor.moveToLast();
            do{
                    historial.add(cursor.getString(0));

            }while(cursor.moveToNext());
        }

        suggestions.clearHistory();

        int i=0;
        for (String s:historial) {

            if (i<10)
                suggestions.saveRecentQuery(s, null);
            else
                break;
            i++;
        }

    }

    private void initializeLocalDataBase() {
        DBOpenHelper dbHelper = new DBOpenHelper(MainActivity.this, "historial", null, 1);
        innerDB = dbHelper.getWritableDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final SearchManager searchManager = (SearchManager) getSystemService(MainActivity.this.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setQueryRefinementEnabled(true);


        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                //Toast.makeText(MainActivity.this,historial.get(position),Toast.LENGTH_SHORT).show();

                Cursor cursor= searchView.getSuggestionsAdapter().getCursor();
                cursor.moveToPosition(position);
                String query =cursor.getString(2);
                searchView.setQuery(query,false);

                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                innerDB.execSQL(String.format("insert into historial values ('%s')",query));
                Cursor cursor = innerDB.rawQuery("SELECT id FROM historial", null);
                if (cursor.moveToFirst()) {
                    int i=0;
                    //cursor.moveToLast();
                    do{
                        System.out.println("**************  "+cursor.getString(0));
                    }while(cursor.moveToNext());
                }


                loadSuggestions();
                progressBar.setVisibility(View.VISIBLE);
                showSearchResults(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    private void showSearchResults(String query) {
        ResponseRestService responseRestService = searchController.searchQuery(query);
        try {
            searchController.getSearchResults(responseRestService,products);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (products.isEmpty())
            Toast.makeText(MainActivity.this,"No hay resultados",Toast.LENGTH_SHORT).show();

        productsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
