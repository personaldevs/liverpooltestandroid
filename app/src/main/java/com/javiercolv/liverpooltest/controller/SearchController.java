package com.javiercolv.liverpooltest.controller;

import android.content.Context;
import android.widget.ProgressBar;

import com.javiercolv.liverpooltest.model.ProductModel;
import com.javiercolv.liverpooltest.services.AsyncTaskConnectService;
import com.javiercolv.liverpooltest.services.ResponseRestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class SearchController {

    private Context context;
    private ProgressBar progressBar;


    public SearchController(Context context, ProgressBar progressBar){
        this.context=context;
        this.progressBar=progressBar;
    }

    public ResponseRestService searchQuery(String query) {
        AsyncTaskConnectService taskConnection = new AsyncTaskConnectService(progressBar);
        ResponseRestService responseRestService = null;
        try {
            responseRestService = taskConnection.execute(query).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return responseRestService;
    }

    public List<ProductModel> getSearchResults(ResponseRestService responseRestService, List<ProductModel> products) throws JSONException {
        products.clear();
        JSONObject obj = new JSONObject(responseRestService.getBody());
        JSONArray searchResults = obj.getJSONObject("plpResults").getJSONArray("records");
        for (int i=0; i<searchResults.length();i++){
            ProductModel product = new ProductModel();
            JSONObject result = (JSONObject) searchResults.get(i);
            product.setPrice(result.getDouble("listPrice"));
            product.setTitle(result.getString("productDisplayName"));
            product.setUrlThumbnail(result.getString("smImage"));
            product.setUrlImage(result.getString("xlImage"));
            products.add(product);
        }
        return products;
    }
}
