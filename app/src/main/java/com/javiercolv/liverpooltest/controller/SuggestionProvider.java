package com.javiercolv.liverpooltest.controller;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class SuggestionProvider extends SearchRecentSuggestionsProvider {
    // AUTHORITY is a unique name, but it is recommended to use the name of the
    // package followed by the name of the class.
    public final static String AUTHORITY = "com.javiercolv.liverpooltest.controller.SuggestionProvider";

    // Uncomment line below, if you want to provide two lines in each suggestion:
    // public final static int MODE = DATABASE_MODE_QUERIES | DATABASE_MODE_2LINES;
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
