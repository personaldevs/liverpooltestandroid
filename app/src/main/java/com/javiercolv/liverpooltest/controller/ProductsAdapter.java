package com.javiercolv.liverpooltest.controller;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javiercolv.liverpooltest.DetailActivity;
import com.javiercolv.liverpooltest.R;
import com.javiercolv.liverpooltest.model.ProductModel;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    Context context;
    List<ProductModel> products;
    public ProductsAdapter(Context context, List<ProductModel> products) {
        this.products=products;
        this.context=context;
    }


    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tittle;
        public TextView price;
        public ImageView imgProduct;

        public ProductViewHolder(View v) {
            super(v);
            tittle=v.findViewById(R.id.txt_name);
            price=v.findViewById(R.id.txt_price);
            imgProduct=v.findViewById(R.id.img_thumbnail);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                ProductModel productModel = products.get(position);
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("product", productModel);
                context.startActivity(intent);
            }
        }
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_item_products, parent, false);


        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int i) {

        holder.tittle.setText(products.get(i).getTitle());
        holder.price.setText(new StringBuilder().append("$").append(new DecimalFormat("#,###,###,###.##").format(products.get(i).getPrice())));

        Glide.with(context)
                .load(products.get(i).getUrlThumbnail())
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgProduct);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

}
