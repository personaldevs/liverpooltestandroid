package com.javiercolv.liverpooltest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class ProductModel implements Parcelable {
    private String title;
    private double price;
    private String urlThumbnail;
    private String urlImage;

    public ProductModel(){}

    public ProductModel(String title, double price, String urlThumbnail, String urlImage) {
        this.title = title;
        this.price = price;
        this.urlThumbnail = urlThumbnail;
        this.urlImage = urlImage;
    }

    protected ProductModel(Parcel in) {
        title = in.readString();
        urlThumbnail = in.readString();
        urlImage = in.readString();
        price = in.readDouble();
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(urlThumbnail);
        parcel.writeString(urlImage);
        parcel.writeDouble(price);

    }

    @Override
    public String toString() {
        return new StringBuilder().append(title).append(" ").append(price).toString();
    }
}
