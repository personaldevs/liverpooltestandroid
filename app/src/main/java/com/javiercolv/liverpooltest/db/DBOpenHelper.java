package com.javiercolv.liverpooltest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class DBOpenHelper extends SQLiteOpenHelper {
    private String createQueries;

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        createQueries = "create table historial(" +
                "id text NOT NULL)";

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createQueries);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists history");
        db.execSQL(createQueries);
    }
}
