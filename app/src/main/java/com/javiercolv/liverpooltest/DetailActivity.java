package com.javiercolv.liverpooltest;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.javiercolv.liverpooltest.model.ProductModel;

import java.text.DecimalFormat;

public class DetailActivity extends AppCompatActivity {

    ImageView imgDetail;
    TextView txtPrice;
    TextView txtTittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imgDetail = findViewById(R.id.img_detail);
        txtPrice = findViewById(R.id.txt_price_detail);
        txtTittle = findViewById(R.id.txt_tittle_detail);
        ProductModel product = getIntent().getParcelableExtra("product");

        txtPrice.setText(new StringBuilder().append("$").append(new DecimalFormat("#,###,###,###.##").format(product.getPrice())));
        txtTittle.setText(product.getTitle());
        Glide.with(this)
                .load(product.getUrlImage())
                .asBitmap()
                .error(R.drawable.ic_launcher_foreground)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imgDetail);
    }

}
