package com.javiercolv.liverpooltest.services;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class ResponseRestService{

    public ResponseRestService(){
        headersResponse = new HashMap();
        body = "";
        bodyError = "";
        responseCode = 0;
    }

    private Map headersResponse;
    private String body;
    private String bodyError;
    private int responseCode;


    public String getBody() {
        return body;
    }

    protected void setBody(String body) {
        this.body = body;
    }

    public String getBodyError() {
        return bodyError;
    }

    protected void setBodyError(String bodyError) {
        this.bodyError = bodyError;
    }

    public Map getHeadersResponse() {
        return headersResponse;
    }

    protected void setHeadersResponse(Map headersResponse) {
        this.headersResponse = headersResponse;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String toString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("headers",headersResponse.toString());
            obj.put("bodyError",bodyError);
            obj.put("body",body);
            obj.put("responseCode",responseCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj.toString();
    }
}

