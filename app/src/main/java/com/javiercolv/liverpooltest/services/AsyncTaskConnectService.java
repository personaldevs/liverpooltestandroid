package com.javiercolv.liverpooltest.services;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class AsyncTaskConnectService extends AsyncTask<String, Integer, ResponseRestService>{

    private ProgressBar progressBar;
    public AsyncTaskConnectService(ProgressBar progressBar){
        this.progressBar=progressBar;
    }

    @Override
    protected ResponseRestService doInBackground(String... strings) {
        publishProgress(10);
        ResponseRestService response= null;
        try {
            response = new RestConnectionService().getRestResponseGET(strings[0]);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(ResponseRestService responseRestService) {
        progressBar.setVisibility(View.GONE);
    }
}
