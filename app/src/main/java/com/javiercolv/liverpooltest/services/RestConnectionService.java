package com.javiercolv.liverpooltest.services;

import org.json.JSONException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Javier Cruz on 06/12/2018.
 */

public class RestConnectionService {

    public ResponseRestService getRestResponseGET(String params) throws JSONException, IOException {
        String endpoint = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v2/plp?search-string=";
        HttpURLConnection conn;
        URL url = new URL(endpoint+params);
        conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        ResponseRestService response = new ResponseRestService();
        response.setHeadersResponse(conn.getHeaderFields());
        response.setResponseCode(conn.getResponseCode());
        if(response.getResponseCode() != 200){
            response.setBodyError(getErrorStream(conn));
        }else{
            response.setBody(getInputStream(conn));
        }

        return response;
    }

    private String getInputStream(HttpURLConnection conn) throws IOException {
        InputStreamReader isr = new InputStreamReader(conn.getInputStream());
        BufferedReader in = new BufferedReader(isr);
        StringBuilder outputString = new StringBuilder();
        String responseString;
        while ((responseString = in.readLine()) != null) {
            outputString.append(responseString);
        }
        return outputString.toString();
    }

    private String getErrorStream(HttpURLConnection conn) throws IOException {
        InputStreamReader isrErr = new InputStreamReader(conn
                .getErrorStream());
        BufferedReader inErr = new BufferedReader(isrErr);
        StringBuilder outputStringErr = new StringBuilder();
        String responseString="";
        System.out.println("Leyendo respuesta Error...");
        while ((responseString = inErr.readLine()) != null) {
            outputStringErr.append(responseString);
        }
        System.out.println("ResponseError: " + outputStringErr);
        return outputStringErr.toString();
    }
}
